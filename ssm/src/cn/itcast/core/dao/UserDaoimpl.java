package cn.itcast.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import cn.itcast.core.pojo.User;

@Controller
public class UserDaoimpl implements UserDao {

	@Autowired
	private SolrServer solrServer;
	
	@Override
	public List<User> findAll() throws Exception {
		SolrQuery solrQuery = new SolrQuery();
		solrQuery.setQuery("*:*");
		QueryResponse queryResponse = solrServer.query(solrQuery);
		SolrDocumentList results = queryResponse.getResults();
		ArrayList<User> users = new ArrayList<>();
		for (SolrDocument document : results) {
			Integer id = Integer.parseInt(String.valueOf(document.get("id")));
			String name = (String) document.get("u_name");
			String birthday = String.valueOf(document.get("u_birthday"));
			User user =new User(id,name,birthday);
			users.add(user);
		}
		return users;
	}

}
