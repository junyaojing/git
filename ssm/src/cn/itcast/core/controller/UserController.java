package cn.itcast.core.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.itcast.core.pojo.User;
import cn.itcast.core.service.UserService;
@Controller
public class UserController {

	@Autowired
	private UserService userService;
	
	@RequestMapping("toAddUser")
	public String toAddUser(){
		return "insert";
	}
  	
	@RequestMapping("insert")
	public String insert(User user){
		userService.insert(user);
		return "redirect:list.action";
	}
	
	@RequestMapping("list")
	public String userList(Model model) throws Exception{
		List<User> userList =userService.finAll();
		model.addAttribute("userList", userList);
		return "list";
	}
}
