package cn.itcast.core.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.itcast.core.dao.UserDao;
import cn.itcast.core.mapper.UserMapper;
import cn.itcast.core.pojo.User;

@Service
@Transactional
public class UserServiceimpl implements UserService {

	@Autowired
	private UserMapper userMapper;
	
	@Autowired
	private UserDao userdao;
	
	@Override
	public void insert(User user) {
		userMapper.insert(user);
	}

	@Override
	public List<User> finAll() throws Exception {
		return userdao.findAll();
	}

}
