package cn.itcast.core.service;

import java.util.List;

import cn.itcast.core.pojo.User;

public interface UserService {

	void insert(User user);

	List<User> finAll() throws Exception;

}
